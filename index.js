//import thử viện express js
const { response, request } = require("express");
const express = require ("express");
//khởi tạo app express
const app = express();
// khai báo mongoose 
var mongoose = require('mongoose');
//khai báo cổng chạy app
const port =  8000;
//khai báo model
const diceHistory = require("./app/models/diceHistoryModel");
const userModel = require("./app/models/userModel");
const prizeModel = require("./app/models/prizeModel");
const voucherModel = require("./app/models/voucherModel");
const prizeHistory = require("./app/models/prizeHistoryModel");
const voucherHistory = require("./app/models/voucherHistoyModel");
//khai báo router
const firstRouter = require("./app/routes/firstRouter");
const userRouter = require("./app/routes/userRouter");
const diceHistoryRouter = require("./app/routes/diceHistoryRouter");
const prizeRouter = require("./app/routes/prizeRouter")
const voucherRouter = require("./app/routes/voucherRouter");
const prizeHistoryRouter = require("./app/routes/prizeHistoryRouter");
const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter");
const findAllConditionRouter = require("./app/routes/findAllConditionRouter");
//kết nối với mongoDB
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_luckyDiceCasino', function(error){
    if(error) throw error;
    console.log('Successfully connected');
});
//cấu hình request đọc 
app.use(express.json());
//app sử dụng router
app.use("/", firstRouter);
app.use("/", userRouter);
app.use("/", diceHistoryRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);
app.use("/", prizeHistoryRouter);
app.use("/", voucherHistoryRouter);
app.use("/", findAllConditionRouter);
//chạy app trên cổng
app.listen(port, () => {
    console.log("App listening on port:" , port);
})




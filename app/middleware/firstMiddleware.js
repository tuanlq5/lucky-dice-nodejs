const { response } = require("express");

const firstMiddleware = (request, response, next) => {
    const today = new Date;
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    console.log(date);
    next();
}
const secondMiddleware = (request, response, next) => {
    console.log(request.method);
    next();
}
module.exports = {
    firstMiddleware,
    secondMiddleware
}
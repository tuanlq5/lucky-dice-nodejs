//import thư viện express
const express = require ("express");

//khai báo router app
const router  = express.Router();

//import controller
const prizeHistoryController = require("../controller/prizeHistoryController");

router.post("/prizeHistory", prizeHistoryController.createPrizeHistory);

router.get("/prizeHistory", prizeHistoryController.getAllPrizeHistory);

router.get("/prizeHistory/:prizeHistoryId", prizeHistoryController.getPrizeHistoryByID);

router.put("/prizeHistory/:prizeHistoryId", prizeHistoryController.updatePrizeHistoryById);

router.delete("/prizeHistory/:prizeHistoryId", prizeHistoryController.deletePrizeHistoryById);


module.exports = router;
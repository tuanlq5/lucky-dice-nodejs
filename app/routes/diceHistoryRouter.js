// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import diceHistory controllers
const diceHistoryController = require('../controller/diceHistoryController');

router.post('/diceHistory', diceHistoryController.creatediceHistory);

router.get('/diceHistory', diceHistoryController.getAllDiceHistory);

router.get("/diceHistory/:diceHistoryId", diceHistoryController.getDiceHistoryByID);

router.put("/diceHistory/:diceHistoryId", diceHistoryController.updateDiceHistoryById);

router.delete("/diceHistory/:diceHistoryId", diceHistoryController.deleteDiceHistoryById);

module.exports = router;
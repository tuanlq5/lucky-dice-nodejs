// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import voucherHistory controllers
const voucherHistoryController = require('../controller/voucherHistoryController');

router.post("/voucherHistory", voucherHistoryController.createVoucherHistory);

router.get('/voucherHistory', voucherHistoryController.getAllVoucherHistory);

router.get("/voucherHistory/:voucherHistoryId", voucherHistoryController.getVoucherHistoryByID);

router.put("/voucherHistory/:voucherHistoryId", voucherHistoryController.updateVoucherHistoryById);

router.delete("/voucherHistory/:voucherHistoryId", voucherHistoryController.deleteVoucherHistoryById);

module.exports = router;
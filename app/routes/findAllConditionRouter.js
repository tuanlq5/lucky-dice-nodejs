// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import diceHistory controllers
const findAllHistoryController = require('../controller/findAllController');

router.get("/dice", findAllHistoryController.findDiceByUser);

router.get("/findPrize", findAllHistoryController.findPrizeHistoryByUser);

router.get("/findVoucherHistory", findAllHistoryController.findVoucherHistoryByUser);

router.post("/devcamp-lucky-dice/dice", findAllHistoryController.randomDice);

router.get("/devcamp-lucky-dice/dice-history", findAllHistoryController.getDiceHistoryByUserName);

router.get("/devcamp-lucky-dice/voucher-history", findAllHistoryController.getVoucherHistoryByUserName);

router.get("/devcamp-lucky-dice/prize-history", findAllHistoryController.getPrizeHistoryByUserName);

module.exports = router;
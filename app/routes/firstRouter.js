//khai báo thư viện express js 
const {response} = require ("express");
const express = require("express");
//khởi tạo path
const path = require("path");
//khai báo router app
const router = express.Router();
//import fisrt middleware
const firstMiddleware = require("../middleware/firstMiddleware");
//load ảnh trong trang web
const appPath = path.dirname(__dirname)
router.use(express.static( appPath + "/views"));

router.get("/", firstMiddleware.firstMiddleware, (request, response) =>{
    response.sendFile(path.join(appPath + "/views/luckyDiceCasino.html"))
});
router.get("/hello", firstMiddleware.secondMiddleware);

module.exports = router;
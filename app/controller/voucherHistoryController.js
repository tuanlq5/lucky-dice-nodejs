// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module VoucherModel 
const voucherHistoryModel = require('../models/voucherHistoyModel');


//   create voucher history : tạo lịch sử voucher
//   input: request body json
//   output: tạo dữ liệu voucherHistory và lưu vào CSDL và trả về response 
const createVoucherHistory = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;  
    // {
    //      "bodyUser" : "637c907a3d1daf3956ad209a",
    //      "bodyVoucher" : "637cf6556dc815dabcd56eae"
    // }
    // B2: kiểm tra dữ liệu
    const ObjectId = require('mongoose').Types.ObjectId;
    if(!ObjectId.isValid(body.bodyUser)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User không hợp lệ!"
        })
    }
    if(!ObjectId.isValid(body.bodyVoucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher không hợp lệ!"
        })
    }
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newVoucherHistory = {
        user: body.bodyUser,
        voucher: body.bodyVoucher
    }
    // Dùng create() thêm dữ liệu vào CSDL
    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Create voucher history Successfully!.",
            data: data
        })
    })
}
/*** Get all voucher: lấy danh sách voucher
 * nhận đầu vào request và response từ router
 * gọi getAllVoucherHistory lấy tất cả dữ liệu user từ CSDL và trả về response.
*/
const getAllVoucherHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    voucherHistoryModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all Vocuher history successfully",
            data: data
        })
    })
}
//   get voucher history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: lấy dữ liệu từ csdl và reponse
const getVoucherHistoryByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const voucherHistoryID =  request.params.voucherHistoryId
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryID)){
     return response.status(400).json({
             status: "Bad request",
             message: "voucherHistory Id không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    voucherHistoryModel.findById(voucherHistoryID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail vocuher history successfull",
             data: data
             })
         }
     )  
}
//   update vocuher history by id : lấy lịch sử voucher theo id
//   input: request params
//   output: update dữ liệu từ csdl và reponse
const updateVoucherHistoryById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const voucherHistoryID =  request.params.voucherHistoryId;
    const body = request.body;
    //bước 2: Validate dữ liệu
    const ObjectId = require('mongoose').Types.ObjectId;
        if(!mongoose.Types.ObjectId.isValid(voucherHistoryID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "Prize History Id không hợp lệ" 
                })
            }  
            if(!ObjectId.isValid(body.bodyUser)) {
                return response.status(400).json({
                    status: "Bad Request",
                    message: "User không hợp lệ!"
                })
            }
            if(!ObjectId.isValid(body.bodyVoucher)) {
                return response.status(400).json({
                    status: "Bad Request",
                    message: "Voucher không hợp lệ!"
                })
            }
            const newVoucherHistory = {
                user: body.bodyUser,
                voucher: body.bodyVoucher
            }
        //bước 3: gọi model tạo dữ liêu
        voucherHistoryModel.findByIdAndUpdate(voucherHistoryID,newVoucherHistory,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update voucher History successfull",
                data: data
                })
            }
        )  
    } 
//   delete vocuher history by id : xóa lịch sử voucher theo id
//   input: request params
//   output: delete dữ liệu từ csdl và reponse
const deleteVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherHistoryID = request.params.voucherHistoryId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher History Id không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    voucherHistoryModel.findByIdAndDelete(voucherHistoryID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete voucher history successfully"
        })
    })
}
module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryByID,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}
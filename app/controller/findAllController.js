// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module VoucherModel 
const voucherHistoryModel = require('../models/voucherHistoyModel');
const diceHistoryModel = require("../models/diceHistoryModel");
const prizeHistoryModel = require("../models/prizeHistoryModel");
const { response } = require("express");
const voucherModel = require("../models/voucherModel");
const userModel = require("../models/userModel");
const prizeModel = require("../models/prizeModel")
const findDiceByUser = (request, response) => {
    //B1: chuẩn bị dữ liệu
    //thu thập dữ liệu front-end 
    let userName = request.query.userName;
    //tạo điều kiện lọc
    let condition = {};
    if(userName){
        condition.user = userName;
    }
    console.log(condition);
    diceHistoryModel
    .find(condition)
    .exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get dice history success",
                data: data
            })
        }
    })  
}
const findPrizeHistoryByUser = (request, response) => {
    //B1: chuẩn bị dữ liệu
    //thu thập dữ liệu front-end 
    let userName = request.query.userName;
    //tạo điều kiện lọc
    let condition = {};
    if(userName){
        condition.user = userName;
    }
    console.log(condition);
    prizeHistoryModel
    .find(condition)
    .exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get prize history success",
                data: data
            })
        }
    })  
}
const findVoucherHistoryByUser = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    //thu thập dữ liệu front-end 
    let userName = request.query.userName;
    //tạo điều kiện lọc
    let condition = {};
    if(userName){
        condition.user = userName;
    }
    console.log(condition);
    voucherHistoryModel
    .find({user:userName})
    .exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get voucher history success",
                data: data
            })
        }
    }) 
    console.log(voucherHistoryModel.find({user:userName}).count());
}
const randomDice = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let body = request.body;
    // var vDataObject = {
    //     firstname: "do",
    //     lastname: "nga",
    //     username: "ngado20"
    // }	
    //B2: validate  dữ liệu
    if(!body.firstname){
        return response.status(400).json({
            status: "Bad Request",
            message: "firstname không hợp lệ"
        })
    }
    if(!body.lastname){
        return response.status(400).json({
            status: "Bad Request",
            message: "lastname không hợp lệ"
        })
    }
    if(!body.username){
        return response.status(400).json({
            status: "Bad Request",
            message: "username không hợp lệ"
        })
    }
    //radom dice from 1 to 6
    const dice = Math.floor(Math.random() * 6) + 1;
    // check nếu user có tồn tại 
    // Nếu user đã tồn tại trong hệ thống, lấy id của user đó để tạo DiceHistory. Ngược lại nếu chưa tồn tại cần tạo 1 user mới và lấy id của user mới đó để tạo DiceHistory
    userModel.findOne({
        userName: body.username
    }, (errorFindUser, userExist) => {
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        } else {
        if (userExist) {
            console.log("User có tồn tại!")
            //tạo data dice history mới
            const newDiceHistory = {
                user: userExist._id,
                dice: dice
            }
            // Dùng create() thêm dữ liệu vào CSDL
            diceHistoryModel.create(newDiceHistory, (error, creatediceHistoryResponseData) => {
                if(error) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: error.message
                    })
                }else{
                    if (dice < 3) {
                        // Nếu dice < 3, không nhận được voucher và prize gì cả
                        return response.status(200).json({
                            dice: dice,
                            prize: null,
                            voucher: null
                        })
                    } else {
                    // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                        voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                            let randomVoucher = Math.floor(Math.random * countVoucher);

                            voucherModel.findOne().skip(randomVoucher).exec((errorFindVoucher, findVoucher) => {
                                // Lưu voucher History
                                voucherHistoryModel.create({
                                    user: userExist._id,
                                    voucher: findVoucher._id
                                }, (errorCreateVoucherHitory, voucherHistoryCreated) => {
                                    if (errorCreateVoucherHitory) {
                                        return response.status(500).json({
                                            status: "Error 500: Internal server error",
                                            message: errorCreateVoucherHitory.message
                                        })
                                    } else {
                                        diceHistoryModel
                                        .find()
                                        .sort({
                                            _id: -1
                                        })
                                        .limit(3)
                                        .exec((errorFindLast3DiceHistory, last3DiceHistory) => {
                                            if (errorFindLast3DiceHistory) {
                                                return response.status(500).json({
                                                    status: "Error 500: Internal server error",
                                                    message: errorFindLast3DiceHistory.message
                                                })
                                            } else {
                                                console.log(last3DiceHistory)
                                                // Kiểm tra 3 dice gần nhất
                                                let checkHavePrize = true;
                                                last3DiceHistory.forEach(diceHistory => {
                                                    if (diceHistory.dice < 3) {
                                                        // Nếu 3 lần gần nhất có 1 lần xúc xắc nhỏ hơn 3 => không nhận được giải thưởng
                                                        checkHavePrize = false;
                                                    }
                                                });
                                                //nếu có không có  giải thưởng thì trả về null
                                                if (!checkHavePrize) {
                                                    return response.status(200).json({
                                                        dice: dice,
                                                        prize: null,
                                                        voucher: findVoucher
                                                    })
                                                }else{ // nếu có thì tiền hành lấy random prize trên prize model
                                                    prizeModel.count().exec((errorCountPrize, countPrize) => {
                                                        let randomPrize = Math.floor(Math.random * countPrize);

                                                        prizeModel.findOne().skip(randomPrize).exec((errorFindPrize, findPrize) => {
                                                            // Lưu prize History
                                                            prizeHistoryModel.create({
                                                                _id: mongoose.Types.ObjectId(),
                                                                user: userExist._id,
                                                                prize: findPrize._id
                                                            }, (errorCreatePrizeHistory, voucherPrizeCreated) => {
                                                                if (errorCreatePrizeHistory) {
                                                                    return response.status(500).json({
                                                                        status: "Error 500: Internal server error",
                                                                        message: errorCreatePrizeHistory.message
                                                                    })
                                                                } else {
                                                                    // Trả về kết quả cuối cùng
                                                                    return response.status(200).json({
                                                                        dice: dice,
                                                                        prize: findPrize,
                                                                        voucher: findVoucher
                                                                    })
                                                                }
                                                            })
                                                        })
                                                    })
                                                }
                                            }
                                        })
                                    }
                                })
                            })
                        })
                    }
                }
        })
    } else {
        console.log("User không tồn tại!")
        //Tạo đối tượng chứa request
        const newUser = {
            userName: body.username,
            firstName: body.firstname,
            lastName: body.lastname
        }
        // Dùng create() thêm dữ liệu vào CSDL
        userModel.create(newUser, (errorCreateUser, createUserData) => { 
            if (errorCreateUser) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorCreateUser.message
                })
            } else{
                const newDiceHistory = {
                    user: createUserData._id,
                    dice: dice
                }
                // Dùng create() thêm diceHistory dữ liệu vào CSDL
                diceHistoryModel.create(newDiceHistory, (errorDiceHistoryCreate, diceHistoryCreated) => {
                    if(errorDiceHistoryCreate) {
                        return response.status(500).json({
                            status: "Internal server error",
                            message: errorDiceHistoryCreate.message
                        })
                    } else  {
                        if (dice < 3) {
                            // Nếu dice < 3, không nhận được voucher và prize gì cả
                            return response.status(200).json({
                                dice: dice,
                                prize: null,
                                voucher: null
                            })
                        } else {
                            // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                            voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                                let randomVoucher = Math.floor(Math.random * countVoucher);

                                voucherModel.findOne().skip(randomVoucher).exec((errorFindVoucher, findVoucher) => {
                                    // Lưu voucher History
                                    voucherHistoryModel.create({
                                        _id: mongoose.Types.ObjectId(),
                                        user: userCreated._id,
                                        voucher: findVoucher._id
                                    }, (errorCreateVoucherHistory, voucherHistoryCreated) => {
                                        if (errorCreateVoucherHistory) {
                                            return response.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: errorCreateVoucherHistory.message
                                            })
                                        } else {
                                            if (errorCreateVoucherHistory) {
                                                return response.status(500).json({
                                                    status: "Error 500: Internal server error",
                                                    message: errorCreateVoucherHistory.message
                                                })
                                            } else {
                                                // User mới không có prize
                                                return response.status(200).json({
                                                    dice: dice,
                                                    prize: null,
                                                    voucher: findVoucher
                                                })
                                            }
                                        }
                                    })
                                })
                            })
                        }
                    } 
                })
            }
        })
    }
}
})
}
// call back function lấy lịch sử ném 
const getDiceHistoryByUserName = (request,response) => {
    // chuẩn bị dữ liệu
    const userName = request.query.username;
    //kiểm tra dữ liệu 
    //xử lý dữ liệu trả về
    if(userName == ""){
        return response.status(400).json({
            status: "User is not found.",
            data: null
        })
    }else{
    userModel.findOne({userName: userName}).exec((errorFindUser, userExist) => {
        console.log(userExist);
        if(errorFindUser) {
            return response.status(500).json({
                status: "Internal server error",
                message: errorFindUser.message
            })
        }
         diceHistoryModel.find({user:userExist._id}).exec((errorFindDiceHistoryByUserName, dataFindDiceHistoryByUserName) => {
                if(errorFindDiceHistoryByUserName) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: errorFindDiceHistoryByUserName.message
                    })
                }
                //trả về response
                return response.status(200).json({
                    status: "Find dice history by username successfully.",
                    data: dataFindDiceHistoryByUserName
                })
            })
        }
    )}
}
// call back function lấy lịch sử vouher
const getVoucherHistoryByUserName = (request,response) => {
    // chuẩn bị dữ liệu
    const userName = request.query.username;
    //kiểm tra dữ liệu 
    //xử lý dữ liệu trả về
    if(userName == ""){
        return response.status(400).json({
            status: "User is not found.",
            data: null
        })
    }else{
    userModel.findOne({userName: userName}).exec((errorFindUser, userExist) => {
        console.log(userExist);
        if(errorFindUser) {
            return response.status(500).json({
                status: "Internal server error",
                message: errorFindUser.message
            })
        }
         voucherHistoryModel.find({user:userExist._id}).populate("voucher").exec((errorFindVoucherHistoryByUserName, dataFindVoucherHistoryByUserName) => {
                if(errorFindVoucherHistoryByUserName) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: errorFindVoucherHistoryByUserName.message
                    })
                }
                //trả về response
                return response.status(200).json({
                    status: "Find voucher history by username successfully.",
                    data: dataFindVoucherHistoryByUserName
                })
            })
        }
    )}
}
// call back function lấy lịch sử ném 
const getPrizeHistoryByUserName = (request,response) => {
    // chuẩn bị dữ liệu
    const userName = request.query.username;
    //kiểm tra dữ liệu 
    //xử lý dữ liệu trả về
    if(userName == ""){
        return response.status(400).json({
            status: "User is not found.",
            data: null
        })
    }else{
    userModel.findOne({userName: userName}).exec((errorFindUser, userExist) => {
        console.log(userExist);
        if(errorFindUser) {
            return response.status(500).json({
                status: "Internal server error",
                message: errorFindUser.message
            })
        }
         prizeHistoryModel.find({user:userExist._id}).populate("prize").exec((errorFindPrizeHistoryByUserName, dataFindPrizeHistoryByUserName) => {
                if(errorFindPrizeHistoryByUserName) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: errorFindPrizeHistoryByUserName.message
                    })
                }
                //trả về response
                return response.status(200).json({
                    status: "Find prize history by username successfully.",
                    data: dataFindPrizeHistoryByUserName
                })
            })
        }
    )}
}
module.exports = {
    findDiceByUser,
    findPrizeHistoryByUser,
    findVoucherHistoryByUser,
    randomDice,
    getDiceHistoryByUserName,
    getVoucherHistoryByUserName,
    getPrizeHistoryByUserName
}
// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module PrizeModel 
const prizeHistoryModel = require('../models/prizeHistoryModel');

//   create prize history : tạo lịch sử phần thưởng
//   input: request body json
//   output: tạo dữ liệu  prizeHistory và lưu vào CSDL và trả về response 
const createPrizeHistory = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;  
    // {
    //      "bodyUser" : "637c907a3d1daf3956ad209a"
    //      "bodyPrize" : "637cf20ce5c37ec265234954"
    // }
    // B2: kiểm tra dữ liệu
    const ObjectId = require('mongoose').Types.ObjectId;
    if(!ObjectId.isValid(body.bodyUser)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User không hợp lệ!"
        })
    }
    if(!ObjectId.isValid(body.bodyPrize)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Prize không hợp lệ!"
        })
    }
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newPrizeHistory = {
        user: body.bodyUser,
        prize: body.bodyPrize
    }
    // Dùng create() thêm dữ liệu vào CSDL
    prizeHistoryModel.create(newPrizeHistory, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Create Prize history Successfully!.",
            data: data
        })
    })
}
/*** Get all dice: lấy danh sách nhận thưởng
 * nhận đầu vào request và response từ router
 * gọi getAllPrizeHistory lấy tất cả dữ liệu user từ CSDL và trả về response.
*/
const getAllPrizeHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    prizeHistoryModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all Prize history successfully",
            data: data
        })
    })
}
//   get prize history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: lấy dữ liệu từ csdl và reponse
const getPrizeHistoryByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const prizeHistoryID =  request.params.prizeHistoryId
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryID)){
     return response.status(400).json({
             status: "Bad request",
             message: "prizeHistory Id không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    prizeHistoryModel.findById(prizeHistoryID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail prize history successfull",
             data: data
             })
         }
     )  
}
//   update prize history by id : lấy lịch sử phần thưởng theo id
//   input: request params
//   output: update dữ liệu từ csdl và reponse
const updatePrizeHistoryById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const prizeHistoryID =  request.params.prizeHistoryId;
    const body = request.body;
    //bước 2: Validate dữ liệu
    const ObjectId = require('mongoose').Types.ObjectId;
        if(!mongoose.Types.ObjectId.isValid(prizeHistoryID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "Prize History Id không hợp lệ" 
                })
            }  
            if(!ObjectId.isValid(body.bodyUser)) {
                return response.status(400).json({
                    status: "Bad Request",
                    message: "User không hợp lệ!"
                })
            }
            if(!ObjectId.isValid(body.bodyPrize)) {
                return response.status(400).json({
                    status: "Bad Request",
                    message: "Prize không hợp lệ!"
                })
            }
            const newPrizeHistory = {
                user: body.bodyUser,
                prize: body.bodyPrize
            }
        //bước 3: gọi model tạo dữ liêu
        prizeHistoryModel.findByIdAndUpdate(prizeHistoryID,newPrizeHistory,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update prize History successfull",
                data: data
                })
            }
        )  
    } 
//   delete prize history by id : xóa lịch sử phần thưởng theo id
//   input: request params
//   output: delete dữ liệu từ csdl và reponse
const deletePrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeHistoryID = request.params.prizeHistoryId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Dice History Id không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    prizeHistoryModel.findByIdAndDelete(prizeHistoryID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete prize history successfully"
        })
    })
}
//export module controller
module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryByID,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}
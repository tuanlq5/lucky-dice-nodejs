// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module UserModel 
const userModel = require('../models/userModel');

/*** Get all user: lấy danh sách khách hàng
 * nhận đầu vào request và response từ router
 * gọi userModel lấy tất cả dữ liệu user từ CSDL và trả về response.
*/
const getAllUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    userModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all user successfully",
            data: data
        })
    })
}

/*** Create User: tạo mới khách hàng
 * nhận đầu vào request và response từ router
 * lấy dữ liệu JSON từ request.body
 * gọi userModel thêm dữ liệu User vào CSDL và trả về response
*/
const createUser = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;
    
    // B2: kiểm tra dữ liệu
    if(!body.bodyUserName || body.bodyUserName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "User Name, không hợp lệ!"
        })
    }
    if(!body.bodyFirstName || body.bodyFirstName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "First Name, không hợp lệ!"
        })
    }
    if(!body.bodyLastName || body.bodyLastName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Last Name, không hợp lệ!"
        })
    }
    
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newUser = {
        userName: body.bodyUserName,
        firstName: body.bodyFirstName,
        lastName: body.bodyLastName
    }
    // Dùng create() thêm dữ liệu vào CSDL
    userModel.create(newUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Successfully! Create a user.",
            data: data
        })
    })
}
const getUserByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const userID =  request.params.userId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userID)){
     return response.status(400).json({
             status: "Bad request",
             message: "userId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    userModel.findById(userID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail user  successfull",
             data: data
             })
         }
     )  
}
const updateUserById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const userID =  request.params.userId;
    const body = request.body;
    //bước 2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(userID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "User Id không hợp lệ" 
                })
            }  
        if(body.bodyUserName == undefined && body.bodyUserName.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "UserName không hợp lệ" 
            })
        }
        if(body.bodyFirstName == undefined && body.bodyFirstName.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "FirstName không hợp lệ" 
            })
        }
        if(body.bodyLastName == undefined && body.bodyLastName.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "LastName không hợp lệ" 
            })
        }
       
        const updateUser= {
            userName: body.bodyUserName,
            firstName: body.bodyFirstName,
            lastName: body.bodyLastName
        }
        //bước 3: gọi model tạo dữ liêu
        userModel.findByIdAndUpdate(userID,updateUser,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update user successfull",
                data: data
                })
            }
        )  
    } 
    const deleteUserById = (request, response) => {
        //B1: chuẩn bị dữ liệu
        const userId = request.params.userId;
    
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(userId)){
            return response.status(400).json({
                status: "Bad Request",
                message: "UserId không hợp lệ"
            })
        } 
        //B3:  Gọi model tạo dữ liệu
        userModel.findByIdAndDelete(userId, (error, data)=> {
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(204).json({
                status: "Delete user successfully"
            })
        })
    }
//export controllers
module.exports = {
    getAllUser, 
    createUser,
    getUserByID,
    updateUserById,
    deleteUserById
} 